#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import re
import jaconv
import csv
import json
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

def main(ns: argparse.Namespace):
    web_url = 'http://www.comitia.co.jp/history/' + str(ns.times) + 'list_sp.html'
    out_file_csv = str(ns.times) + '.csv'
    out_file_js = str(ns.times) + '.json'
    print(str(ns.json))

    print("Target URL: " + web_url)
    print("Output csv file name: " + out_file_csv)

    html = urlopen(web_url)
    bsObj = BeautifulSoup(html, "html.parser")

    table = bsObj.findAll("table")[0]
    rows = table.findAll("tr")

    with open(out_file_csv, "w", encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow({'circle', 'space'})
        for row in rows:
            csvRow = []
            for cell in row.findAll(['td', 'th']):
                csvRow.append(jaconv.z2h(cell.get_text(), kana=False, digit=True, ascii=True))
            writer.writerow(csvRow)

    # 本当はcsv.writeの時点でいい感じに排除したかったけどできなかったので・・・
    deleteIndex(out_file_csv)

    # json出力する
    if (ns.json == True):
        print("Output json file name: " + out_file_js)
        csv2js(out_file_csv, out_file_js)

    print("Done.")

def deleteIndex(file_name):
    f = open(file_name)
    output = []
    for line in f:
        if not re.compile('^(.|展示)$').search(line):
            output.append(line)
    f.close()
    f = open(file_name, "w")
    f.writelines(output)
    f.close()

def csv2js(file_name_csv, file_name_js):
    js_list = []

    with open(file_name_csv, 'r', encoding='utf-8') as f:
        for row in csv.DictReader(f):
            js_list.append(row)

    with open(file_name_js, 'w', encoding='utf-8') as f:
        json.dump(js_list, f, ensure_ascii=False,
                              indent=4,
                              sort_keys=True,
                              separators=(',', ': '))


if __name__ == "__main__":
    # 引数の設定
    parser: argparse.ArgumentParser = argparse.ArgumentParser()
    parser.add_argument('times',
                        help='number of times held comitia.', type=int)
    parser.add_argument('--json', '-j',
                        help='output json format file', action='store_true')
    ns: argparse.Namespace = parser.parse_args()

    main(ns)
