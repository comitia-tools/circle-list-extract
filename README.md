# Circle list extract

コミティアのwebページからサークルリストを抽出するツール

## これはなに

コミティアのwebページからサークルリストを取得し、csv、jsonを出力するツールです。
コミティア115以降に対応しています。

## How to use

`python main.py [(--json)] [開催回] `

```
usage: main.py [-h] [--json] times

positional arguments:
  times       number of times held comitia.

optional arguments:
  -h, --help  show this help message and exit
  --json, -j  output json format file
```

### 動作確認環境

- Ubuntu 18.04.3 LTS 64bit
- Python 3.6.8


### 必要なパッケージ

必要なパッケージはrements.txtに記述しています。  
`pip install -r requirements.txt` で一括インストール可能です。

## LICENSE

WTFPL
